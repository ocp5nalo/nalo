<?php
// src/NaloBundle/Controller/AccueilController.php

namespace NaloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AccueilController extends Controller
{
    public function indexAction()
    {
        $content = $this->get('templating')->render('NaloBundle:Accueil:index.html.twig', array('nom' => 'La team OC-P5-NALO'));
        return new Response($content);
    }
}
